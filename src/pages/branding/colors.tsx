import React from "react";
import Layout from "components/layout";
import Section from "components/section";
import { ColorCardItem, ColorCards } from "components/colorcard";

export default function PageColors() {
    return (
        <Layout title="Color Themes" subTitle="Theming">
            <Section title="Grayscale">
                <ColorCards data={grayScale} />
            </Section>
            <Section title="Colors">
                <ColorCards data={colors} />
            </Section>
        </Layout>
    );
}

const colors: ColorCardItem[] = [
    {
        title: "Primary",
        subTitle: "Used as the primary color.",
        bgClass: "bg-primary",
        variantColors: ["bg-primary--dark", "bg-primary--darkmode"],
    },
    {
        title: "Secondary",
        subTitle: "Used for accents & actions.",
        bgClass: "bg-secondary",
        variantColors: ["bg-secondary--dark", "bg-secondary--darkmode"],
    },
    {
        title: "Error",
        subTitle: "Used for error states.",
        bgClass: "bg-error",
        variantColors: ["bg-error--dark", "bg-error--darkmode"],
    },
    {
        title: "Success",
        subTitle: "Used for success states.",
        bgClass: "bg-success",
        variantColors: ["bg-success--dark", "bg-success--darkmode"],
    },
    {
        title: "Warning",
        subTitle: "Used to represent caution.",
        bgClass: "bg-warning",
        variantColors: ["bg-warning--dark", "bg-warning--darkmode"],
    },
    {
        title: "Gradient Primary",
        subTitle: "The primary gradient.",
        bgClass: "bg-gradient--primary",
        variantColors: ["bg-primary", "bg-primary--darkmode"],
    },
    {
        title: "Gradient Secondary",
        subTitle: "The secondary gradient.",
        bgClass: "bg-gradient--secondary",
        variantColors: ["bg-primary", "bg-secondary--darkmode"],
    },
    {
        title: "Gradient Accent",
        subTitle: "An accent gradient.",
        bgClass: "bg-gradient--accent",
        variantColors: ["bg-error", "bg-warning--darkmode"],
    },
];

const grayScale = [
    {
        title: "Title-Active",
        subTitle: "Used for both links and titles.",
        bgClass: "bg-title-active",
    },
    {
        title: "Body",
        subTitle: "Used for body copy text",
        bgClass: "bg-body",
    },
    {
        title: "Label",
        subTitle: "Used for the label text.",
        bgClass: "bg-label",
    },
    {
        title: "Placeholder",
        subTitle: "Used for initial copy of inputs.",
        bgClass: "bg-placeholder",
    },
    {
        title: "Line",
        subTitle: "Used for line based elements.",
        bgClass: "bg-line",
    },
    {
        title: "Input Background",
        subTitle: "Used for input bg accessibility.",
        bgClass: "bg-input-bg",
    },
    {
        title: "Background",
        subTitle: "Used for element backgrounds.",
        bgClass: "bg-background",
    },
    {
        title: "Off-white",
        subTitle: "Used to avoid darkmode strain.",
        bgClass: "bg-off-white",
    },
];
