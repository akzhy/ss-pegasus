import Layout from "components/layout";
import Section from "components/section";
import React, { ReactNode } from "react";

export default function PageDesktopTypography() {
    return (
        <Layout title="Desktop Typography" subTitle="Theming">
            <Section title="Display">
                <div className="typography">
                    <div className="typography__items">
                        <TypographyItem label="Display Large">
                            <h1 className="heading">
                                The future is in our hands to shape.
                            </h1>
                        </TypographyItem>
                        <TypographyItem label="Display Medium">
                            <h1 className="heading heading--md">
                                The future is in our hands to shape.
                            </h1>
                        </TypographyItem>
                        <TypographyItem label="Display Small">
                            <h1 className="heading heading--sm">
                                The future is in our hands to shape.
                            </h1>
                        </TypographyItem>
                    </div>
                    <div className="typography__items">
                        <TypographyItem label="Display Large">
                            <h1 className="heading heading--bold">
                                The future is in our hands to shape.
                            </h1>
                        </TypographyItem>
                        <TypographyItem label="Display Medium">
                            <h1 className="heading heading--md heading--bold">
                                The future is in our hands to shape.
                            </h1>
                        </TypographyItem>
                        <TypographyItem label="Display Small">
                            <h1 className="heading heading--sm heading--bold">
                                The future is in our hands to shape.
                            </h1>
                        </TypographyItem>
                    </div>
                </div>
            </Section>
            <Section title="Text">
                <div className="typography">
                    <div className="typography__items">
                        <TypographyItem label="Text Large">
                            <p className="text text--lg">
                                The future is in our hands to shape.
                            </p>
                        </TypographyItem>
                        <TypographyItem label="Text Medium">
                            <p className="text">
                                The future is in our hands to shape.
                            </p>
                        </TypographyItem>
                        <TypographyItem label="Text Small">
                            <p className="text text--sm">
                                The future is in our hands to shape.
                            </p>
                        </TypographyItem>
                        <TypographyItem label="Text Extra Small">
                            <p className="text text--xs">
                                The future is in our hands to shape.
                            </p>
                        </TypographyItem>
                    </div>
                    <div className="typography__items">
                        <TypographyItem label="Text Large">
                            <a href="#!" className="text text--lg">
                                The future is in our hands to shape.
                            </a>
                        </TypographyItem>
                        <TypographyItem label="Text Medium">
                            <a href="#!" className="text">
                                The future is in our hands to shape.
                            </a>
                        </TypographyItem>
                        <TypographyItem label="Text Small">
                            <a href="#!" className="text text--sm">
                                The future is in our hands to shape.
                            </a>
                        </TypographyItem>
                        <TypographyItem label="Text Extra Small">
                            <a href="#!" className="text text--xs">
                                The future is in our hands to shape.
                            </a>
                        </TypographyItem>
                    </div>
                </div>
            </Section>
        </Layout>
    );
}

const TypographyItem = ({
    label,
    children,
}: {
    label: string;
    children: ReactNode;
}) => {
    return (
        <div className="typography__item">
            <p className="typography__item__label color-label text--xs">
                {label}
            </p>
            {children}
        </div>
    );
};
