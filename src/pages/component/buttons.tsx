import React from "react";
import Layout from "components/layout";
import Section from "components/section";
import { Button } from "components/button";

export default function PageButtons() {
    return (
        <Layout title="Buttons" subTitle="Forms">
            <Section title="Large">
                <div className="buttons">
                    <div className="buttons__row color-label">
                        <div className="buttons__col"></div>
                        <div className="buttons__col">Primary</div>
                        <div className="buttons__col">Secondary</div>
                        <div className="buttons__col">Subtle</div>
                        <div className="buttons__col">Text</div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Initial</div>
                        <div className="buttons__col">
                            <Button label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button label="Button" actionType="secondary" />
                        </div>
                        <div className="buttons__col">
                            <Button label="Button" actionType="subtle" />
                        </div>
                        <div className="buttons__col">
                            <Button label="Button" actionType="text" />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Hover</div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="subtle"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="text"
                                className="button--primary--state-hover"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Focus</div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="subtle"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="text"
                                className="button--state-focus"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Active</div>
                        <div className="buttons__col">
                            <Button label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button label="Button" actionType="secondary" />
                        </div>
                        <div className="buttons__col">
                            <Button label="Button" actionType="subtle" />
                        </div>
                        <div className="buttons__col">
                            <Button label="Button" actionType="text" />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Loading</div>
                        <div className="buttons__col">
                            <Button label="Button" isLoading={true} />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="subtle"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="text"
                                isLoading={true}
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Disabled</div>
                        <div className="buttons__col">
                            <Button label="Button" disabled={true} />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="subtle"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="text"
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
            </Section>
            <Section title="Medium">
                <div className="buttons">
                    <div className="buttons__row color-label">
                        <div className="buttons__col"></div>
                        <div className="buttons__col">Primary</div>
                        <div className="buttons__col">Secondary</div>
                        <div className="buttons__col">Subtle</div>
                        <div className="buttons__col">Text</div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Initial</div>
                        <div className="buttons__col">
                            <Button size="medium" label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="secondary"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="subtle"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="text"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Hover</div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="secondary"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="subtle"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="text"
                                className="button--primary--state-hover"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Focus</div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="secondary"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="subtle"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="text"
                                className="button--state-focus"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Active</div>
                        <div className="buttons__col">
                            <Button size="medium" label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="secondary"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="subtle"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="text"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Loading</div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="secondary"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="subtle"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="text"
                                isLoading={true}
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Disabled</div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="secondary"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="subtle"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="medium"
                                label="Button"
                                actionType="text"
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
            </Section>
            <Section title="Small">
                <div className="buttons">
                    <div className="buttons__row color-label">
                        <div className="buttons__col"></div>
                        <div className="buttons__col">Primary</div>
                        <div className="buttons__col">Secondary</div>
                        <div className="buttons__col">Subtle</div>
                        <div className="buttons__col">Text</div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Initial</div>
                        <div className="buttons__col">
                            <Button size="small" label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="secondary"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="subtle"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="text"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Hover</div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="secondary"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="subtle"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="text"
                                className="button--primary--state-hover"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Focus</div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="secondary"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="subtle"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="text"
                                className="button--state-focus"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Active</div>
                        <div className="buttons__col">
                            <Button size="small" label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="secondary"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="subtle"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="text"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Loading</div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="secondary"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="subtle"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="text"
                                isLoading={true}
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Disabled</div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="secondary"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="subtle"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                size="small"
                                label="Button"
                                actionType="text"
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
            </Section>
            <Section title="Icon">
                <div className="buttons">
                    <div className="buttons__row color-label">
                        <div className="buttons__col"></div>
                        <div className="buttons__col">Primary</div>
                        <div className="buttons__col">Secondary</div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Initial</div>
                        <div className="buttons__col">
                            <Button iconOnly={true} label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                iconOnly={true}
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Hover</div>
                        <div className="buttons__col">
                            <Button
                                iconOnly={true}
                                label="Button"
                                className="button--primary--state-hover"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                iconOnly={true}
                                className="button--secondary--state-hover"
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Focus</div>
                        <div className="buttons__col">
                            <Button
                                iconOnly={true}
                                label="Button"
                                className="button--state-focus"
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                className="button--state-focus"
                                iconOnly={true}
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Active</div>
                        <div className="buttons__col">
                            <Button iconOnly={true} label="Button" />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                iconOnly={true}
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Loading</div>
                        <div className="buttons__col">
                            <Button
                                iconOnly={true}
                                label="Button"
                                isLoading={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                iconOnly={true}
                                isLoading={true}
                            />
                        </div>
                    </div>
                    <div className="buttons__row">
                        <div className="buttons__col color-label">Disabled</div>
                        <div className="buttons__col">
                            <Button
                                iconOnly={true}
                                label="Button"
                                disabled={true}
                            />
                        </div>
                        <div className="buttons__col">
                            <Button
                                label="Button"
                                actionType="secondary"
                                iconOnly={true}
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
            </Section>
        </Layout>
    );
}
