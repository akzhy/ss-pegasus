import React from "react";
import Layout from "components/layout";
import Section from "components/section";
import { TextInput } from "components/text_input";
import { ProfileIcon, SearchIcon } from "components/icons";
import { TextArea } from "components/text_area";

export default function PageTextInputs() {
    return (
        <Layout title="Text Inputs" subTitle="Forms">
            <Section title="Input - Large">
                <div className="inputs">
                    <div className="inputs__row color-label">
                        <div className="inputs__col"></div>
                        <div className="inputs__col">Standard</div>
                        <div className="inputs__col">Search / Icon</div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Initial</div>
                        <div className="inputs__col">
                            <TextInput label="Phone Number" />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Active</div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                parentClassName="textinput--focused"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                parentClassName="textinput--focused"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Typing</div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                defaultValue="999-999-9"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                defaultValue="999-999-9"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Filled</div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                defaultValue="999-999-999"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                defaultValue="999-999-999"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Disabled</div>
                        <div className="inputs__col">
                            <TextInput label="Phone Number" disabled={true} />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                disabled={true}
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Caption</div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                caption="Here's a hint for you"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                caption="Here's a hint for you"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Success</div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                caption="Success Message"
                                inputState="success"
                                defaultValue="999-999-999"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Username"
                                caption="Username available!!"
                                inputState="success"
                                defaultValue="user"
                                icon={<ProfileIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Error</div>
                        <div className="inputs__col">
                            <TextInput
                                label="Phone Number"
                                caption="Error Message"
                                inputState="error"
                                defaultValue="999-999-999"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Username"
                                caption="Username taken."
                                inputState="error"
                                defaultValue="user"
                                icon={<ProfileIcon />}
                            />
                        </div>
                    </div>
                </div>
            </Section>
            <Section title="Input - Small">
                <div className="inputs">
                    <div className="inputs__row color-label">
                        <div className="inputs__col"></div>
                        <div className="inputs__col">Standard</div>
                        <div className="inputs__col">Search / Icon</div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Initial</div>
                        <div className="inputs__col">
                            <TextInput inputSize="small" label="Phone Number" />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Active</div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                parentClassName="textinput--focused"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                parentClassName="textinput--focused"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Typing</div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                defaultValue="999-999-9"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                defaultValue="999-999-9"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Filled</div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                defaultValue="999-999-999"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                defaultValue="999-999-999"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Disabled</div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                disabled={true}
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                disabled={true}
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Caption</div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                caption="Here's a hint for you"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                caption="Here's a hint for you"
                                icon={<SearchIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Success</div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                caption="Success Message"
                                inputState="success"
                                defaultValue="999-999-999"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Username"
                                caption="Username available!!"
                                inputState="success"
                                defaultValue="user"
                                icon={<ProfileIcon />}
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Error</div>
                        <div className="inputs__col">
                            <TextInput
                                inputSize="small"
                                label="Phone Number"
                                caption="Error Message"
                                inputState="error"
                                defaultValue="999-999-999"
                            />
                        </div>
                        <div className="inputs__col">
                            <TextInput
                                label="Username"
                                caption="Username taken."
                                inputState="error"
                                defaultValue="user"
                                icon={<ProfileIcon />}
                            />
                        </div>
                    </div>
                </div>
            </Section>
            <Section title="Textarea">
                <div className="inputs">
                    <div className="inputs__row color-label">
                        <div className="inputs__col"></div>
                        <div className="inputs__col">Standard</div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Initial</div>
                        <div className="inputs__col">
                            <TextArea label="Share a Reply" />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Active</div>
                        <div className="inputs__col">
                            <TextArea
                                label="Share a Reply"
                                parentClassName="textarea--focused"
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Typing</div>
                        <div className="inputs__col">
                            <TextArea
                                label="Share a Reply"
                                defaultValue="Great article"
                                parentClassName="textarea--focused"
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Filled</div>
                        <div className="inputs__col">
                            <TextArea
                                label="Share a Reply"
                                defaultValue="Great article. I wonder if there is a way this can be learned?"
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Disabled</div>
                        <div className="inputs__col">
                            <TextArea disabled={true} label="Share a Reply" />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Caption</div>
                        <div className="inputs__col">
                            <TextArea
                                label="Share a Reply"
                                caption="Here’s a hint that might help you."
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Success</div>
                        <div className="inputs__col">
                            <TextArea
                                label="Share a Reply"
                                inputState="success"
                                caption="Success Message"
                                defaultValue="Great article. I wonder if there is a way this can be learned?"
                            />
                        </div>
                    </div>
                    <div className="inputs__row">
                        <div className="inputs__col color-label">Error</div>
                        <div className="inputs__col">
                            <TextArea
                                label="Share a Reply"
                                inputState="error"
                                caption="Error Message"
                                defaultValue="Great article. I wonder if there is a way this can be learned?"
                            />
                        </div>
                    </div>
                </div>
            </Section>
        </Layout>
    );
}
