import React from "react";
import Layout from "components/layout";
import Section from "components/section";
import { CheckBox } from "components/checkbox";
import { RadioButton } from "components/radiobutton";
import { ToggleButton } from "components/togglebutton";

export default function PageCustomInputs() {
    return (
        <Layout title="Custom Inputs" subTitle="Forms">
            <Section title="Checkboxes">
                <div className="custom-inputs">
                    <div className="custom-inputs__row color-label">
                        <div className="custom-inputs__col"></div>
                        <div className="custom-inputs__col">Checked</div>
                        <div className="custom-inputs__col">Unchecked</div>
                    </div>
                </div>
                <div className="custom-inputs">
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Initial
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox defaultChecked={true} />
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox defaultChecked={false} />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Hover
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox
                                defaultChecked={true}
                                parentClassName="checkbox--state-hover"
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox
                                defaultChecked={false}
                                parentClassName="checkbox--state-hover"
                            />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Focus
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox
                                defaultChecked={true}
                                parentClassName="checkbox--state-focus"
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox
                                defaultChecked={false}
                                parentClassName="checkbox--state-focus"
                            />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Disabled
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox defaultChecked={true} disabled={true} />
                        </div>
                        <div className="custom-inputs__col">
                            <CheckBox defaultChecked={false} disabled={true} />
                        </div>
                    </div>
                </div>
            </Section>
            <Section title="Radios">
                <div className="custom-inputs">
                    <div className="custom-inputs__row color-label">
                        <div className="custom-inputs__col"></div>
                        <div className="custom-inputs__col">Checked</div>
                        <div className="custom-inputs__col">Unchecked</div>
                    </div>
                </div>
                <div className="custom-inputs">
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Initial
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton defaultChecked={true} />
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton defaultChecked={false} />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Hover
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton
                                defaultChecked={true}
                                parentClassName="radio--state-hover"
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton
                                defaultChecked={false}
                                parentClassName="radio--state-hover"
                            />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Focus
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton
                                defaultChecked={true}
                                parentClassName="radio--state-focus"
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton
                                defaultChecked={false}
                                parentClassName="radio--state-focus"
                            />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Disabled
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton
                                defaultChecked={true}
                                disabled={true}
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <RadioButton
                                defaultChecked={false}
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
            </Section>
            <Section title="Toggles">
                <div className="custom-inputs">
                    <div className="custom-inputs__row color-label">
                        <div className="custom-inputs__col"></div>
                        <div className="custom-inputs__col">Checked</div>
                        <div className="custom-inputs__col">Unchecked</div>
                    </div>
                </div>
                <div className="custom-inputs">
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Initial
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton defaultChecked={true} />
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton defaultChecked={false} />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Hover
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton
                                defaultChecked={true}
                                parentClassName="toggle--state-hover"
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton
                                defaultChecked={false}
                                parentClassName="toggle--state-hover"
                            />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Focus
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton
                                defaultChecked={true}
                                parentClassName="toggle--state-focus"
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton
                                defaultChecked={false}
                                parentClassName="toggle--state-focus"
                            />
                        </div>
                    </div>
                    <div className="custom-inputs__row ">
                        <div className="custom-inputs__col color-label">
                            Disabled
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton
                                defaultChecked={true}
                                disabled={true}
                            />
                        </div>
                        <div className="custom-inputs__col">
                            <ToggleButton
                                defaultChecked={false}
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
            </Section>
        </Layout>
    );
}
