import React from "react";
import { Story, Meta } from "@storybook/react";
import { ToggleButton, ToggleButtonProps } from "components/togglebutton";

export default {
    title: "Example/ToggleButton",
    component: ToggleButton,
} as Meta;

const Template: Story<ToggleButtonProps> = (args) => <ToggleButton {...args} />;

export const Default = Template.bind({});
Default.args = {
    checked: false,
};

export const Checked = Template.bind({});
Checked.args = {
    checked: true,
};
