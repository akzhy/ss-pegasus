import React from "react";
import { Story, Meta } from "@storybook/react";
import { CheckBox, CheckBoxProps } from "components/checkbox";

export default {
    title: "Example/CheckBox",
    component: CheckBox,
} as Meta;

const Template: Story<CheckBoxProps> = (args) => <CheckBox {...args} />;

export const Default = Template.bind({});
Default.args = {
    checked: false,
};

export const Checked = Template.bind({});
Checked.args = {
    checked: true,
};
