import React from "react";
import { Story, Meta } from "@storybook/react";

import { TextInput, TextInputProps } from "components/text_input";
import { ProfileIcon, SearchIcon } from "components/icons";

export default {
    title: "Example/TextInput",
    component: TextInput,
} as Meta;

const Template: Story<TextInputProps> = (args) => <TextInput {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: "Label",
};

export const Icon = Template.bind({});
Icon.args = {
    label: "Label",
    caption: "Caption",
    icon: <SearchIcon />,
};

export const Caption = Template.bind({});
Caption.args = {
    label: "Label",
    caption: "Caption",
};

export const Success = Template.bind({});
Success.args = {
    label: "Label",
    inputState: "success",
    caption: "Success Message",
    defaultValue: "Value",
    icon: <ProfileIcon />,
};

export const Error = Template.bind({});
Error.args = {
    label: "Label",
    inputState: "error",
    caption: "Error Message",
    defaultValue: "Value",
    icon: <ProfileIcon />,
};
