import React from "react";
import { Story, Meta } from "@storybook/react";

import { Button, ButtonProps } from "components/button";

export default {
    title: "Example/Button",
    component: Button,
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    actionType: "primary",
    label: "Button",
};

export const Secondary = Template.bind({});
Secondary.args = {
    label: "Button",
    actionType: "secondary",
};

export const Subtle = Template.bind({});
Subtle.args = {
    actionType: "subtle",
    size: "large",
    label: "Button",
};

export const Text = Template.bind({});
Text.args = {
    size: "small",
    actionType: "text",
    label: "Button",
};
