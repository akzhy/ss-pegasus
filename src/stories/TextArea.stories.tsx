import React from "react";
import { Story, Meta } from "@storybook/react";
import { TextArea, TextAreaProps } from "components/text_area";

export default {
    title: "Example/TextArea",
    component: TextArea,
} as Meta;

const Template: Story<TextAreaProps> = (args) => <TextArea {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: "Label",
};

export const Icon = Template.bind({});
Icon.args = {
    label: "Label",
    caption: "Caption",
};

export const Caption = Template.bind({});
Caption.args = {
    label: "Label",
    caption: "Caption",
};

export const Success = Template.bind({});
Success.args = {
    label: "Label",
    inputState: "success",
    caption: "Success Message",
    defaultValue: "Value",
};

export const Error = Template.bind({});
Error.args = {
    label: "Label",
    inputState: "error",
    caption: "Error Message",
    defaultValue: "Value",
};
