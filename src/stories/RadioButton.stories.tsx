import React from "react";
import { Story, Meta } from "@storybook/react";
import { RadioButton, RadioButtonProps } from "components/radiobutton";

export default {
    title: "Example/RadioButton",
    component: RadioButton,
} as Meta;

const Template: Story<RadioButtonProps> = (args) => <RadioButton {...args} />;

export const Default = Template.bind({});
Default.args = {
    checked: false,
};

export const Checked = Template.bind({});
Checked.args = {
    checked: true,
};
