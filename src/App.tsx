import React from "react";
import PageColors from "pages/branding/colors";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./sass/main.scss";
import PageDesktopTypography from "pages/branding/desktop_typography";
import PageMobileTypography from "pages/branding/mobile_typography";
import PageIcons from "pages/branding/icons";
import PageButtons from "pages/component/buttons";
import PageTextInputs from "pages/component/text_inputs";
import PageCustomInputs from "pages/component/custom_inputs";

function App() {
    return (
        <Router>
            <Route path="/" exact>
                <PageColors />
            </Route>
            <Route path="/desktop-typography" exact>
                <PageDesktopTypography />
            </Route>
            <Route path="/mobile-typography" exact>
                <PageMobileTypography />
            </Route>
            <Route path="/icons" exact>
                <PageIcons />
            </Route>
            <Route path="/components" exact>
                <PageButtons />
            </Route>
            <Route path="/components/textinputs" exact>
                <PageTextInputs />
            </Route>
            <Route path="/components/custominputs" exact>
                <PageCustomInputs />
            </Route>
        </Router>
    );
}

export default App;
