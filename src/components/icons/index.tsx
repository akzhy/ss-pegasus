import { ReactComponent as AlertInfoIcon } from "./alert-info.svg";
import { ReactComponent as AtSignIcon } from "./at-sign.svg";
import { ReactComponent as AttachIcon } from "./attach.svg";
import { ReactComponent as BackArrowIcon } from "./back-arrow.svg";
import { ReactComponent as BackIcon } from "./back.svg";
import { ReactComponent as BackspaceIcon } from "./backspace.svg";
import { ReactComponent as BackwardIcon } from "./backward.svg";
import { ReactComponent as BagIcon } from "./bag.svg";
import { ReactComponent as BookmarkIcon } from "./bookmark.svg";
import { ReactComponent as CalendarIcon } from "./calendar.svg";
import { ReactComponent as CallIcon } from "./call.svg";
import { ReactComponent as CameraIcon } from "./camera.svg";
import { ReactComponent as CardIcon } from "./card.svg";
import { ReactComponent as CartIcon } from "./cart.svg";
import { ReactComponent as CastIcon } from "./cast.svg";
import { ReactComponent as ChartBarIcon } from "./chart-bar.svg";
import { ReactComponent as ChartPieIcon } from "./chart-pie.svg";
import { ReactComponent as CheckIcon } from "./check.svg";
import { ReactComponent as ChipIcon } from "./chip.svg";
import { ReactComponent as CloseIcon } from "./close.svg";
import { ReactComponent as CommentIcon } from "./comment.svg";
import { ReactComponent as ConfigureIcon } from "./configure.svg";
import { ReactComponent as CopyIcon } from "./copy.svg";
import { ReactComponent as CutIcon } from "./cut.svg";
import { ReactComponent as DeleteIcon } from "./delete.svg";
import { ReactComponent as DisabledIcon } from "./disabled.svg";
import { ReactComponent as EditIcon } from "./edit.svg";
import { ReactComponent as EmojiSmileIcon } from "./emoji-smile.svg";
import { ReactComponent as ExportIcon } from "./export.svg";
import { ReactComponent as FileIcon } from "./file.svg";
import { ReactComponent as FolderIcon } from "./folder.svg";
import { ReactComponent as ForwardArrowIcon } from "./forward-arrow.svg";
import { ReactComponent as ForwardIcon } from "./forward.svg";
import { ReactComponent as HeartIcon } from "./heart.svg";
import { ReactComponent as HideIcon } from "./hide.svg";
import { ReactComponent as HomeIcon } from "./home.svg";
import { ReactComponent as ImageIcon } from "./image.svg";
import { ReactComponent as LayoutIcon } from "./layout.svg";
import { ReactComponent as LinkIcon } from "./link.svg";
import { ReactComponent as LoadingIcon } from "./loading.svg";
import { ReactComponent as LocationIcon } from "./location.svg";
import { ReactComponent as LockIcon } from "./lock.svg";
import { ReactComponent as MailIcon } from "./mail.svg";
import { ReactComponent as MapIcon } from "./map.svg";
import { ReactComponent as MenuLeftIcon } from "./menu-left.svg";
import { ReactComponent as MenuRightIcon } from "./menu-right.svg";
import { ReactComponent as MicrophoneIcon } from "./microphone.svg";
import { ReactComponent as MoreVertIcon } from "./more-vert.svg";
import { ReactComponent as MoreIcon } from "./more.svg";
import { ReactComponent as NotificationIcon } from "./notification.svg";
import { ReactComponent as PasteIcon } from "./paste.svg";
import { ReactComponent as PauseIcon } from "./pause.svg";
import { ReactComponent as PlayIcon } from "./play.svg";
import { ReactComponent as PlusIcon } from "./plus.svg";
import { ReactComponent as PrinterIcon } from "./printer.svg";
import { ReactComponent as ProfileIcon } from "./profile.svg";
import { ReactComponent as RefreshIcon } from "./refresh.svg";
import { ReactComponent as ResizeIcon } from "./resize.svg";
import { ReactComponent as ScrollIcon } from "./scroll.svg";
import { ReactComponent as SearchIcon } from "./search.svg";
import { ReactComponent as SendIcon } from "./send.svg";
import { ReactComponent as SettingsIcon } from "./settings.svg";
import { ReactComponent as SoundMutedIcon } from "./sound-muted.svg";
import { ReactComponent as SoundPlayingIcon } from "./sound-playing.svg";
import { ReactComponent as StartIcon } from "./start.svg";
import { ReactComponent as TagIcon } from "./tag.svg";
import { ReactComponent as ThumbsDownIcon } from "./thumbs-down.svg";
import { ReactComponent as ThumbsUpIcon } from "./thumbs-up.svg";
import { ReactComponent as TimeIcon } from "./time.svg";
import { ReactComponent as ViewIcon } from "./view.svg";
import { ReactComponent as WifiIcon } from "./wifi.svg";
import { ReactComponent as ChevronRight } from "./chevron-right.svg";

export {
    AlertInfoIcon,
    AtSignIcon,
    AttachIcon,
    BackArrowIcon,
    BackIcon,
    BackspaceIcon,
    BackwardIcon,
    BagIcon,
    BookmarkIcon,
    CalendarIcon,
    CallIcon,
    CameraIcon,
    CardIcon,
    CartIcon,
    CastIcon,
    ChartBarIcon,
    ChartPieIcon,
    CheckIcon,
    ChipIcon,
    CloseIcon,
    CommentIcon,
    ConfigureIcon,
    CopyIcon,
    CutIcon,
    DeleteIcon,
    DisabledIcon,
    EditIcon,
    EmojiSmileIcon,
    ExportIcon,
    FileIcon,
    FolderIcon,
    ForwardArrowIcon,
    ForwardIcon,
    HeartIcon,
    HideIcon,
    HomeIcon,
    ImageIcon,
    LayoutIcon,
    LinkIcon,
    LoadingIcon,
    LocationIcon,
    LockIcon,
    MailIcon,
    MapIcon,
    MenuLeftIcon,
    MenuRightIcon,
    MicrophoneIcon,
    MoreVertIcon,
    MoreIcon,
    NotificationIcon,
    PasteIcon,
    PauseIcon,
    PlayIcon,
    PlusIcon,
    PrinterIcon,
    ProfileIcon,
    RefreshIcon,
    ResizeIcon,
    ScrollIcon,
    SearchIcon,
    SendIcon,
    SettingsIcon,
    SoundMutedIcon,
    SoundPlayingIcon,
    StartIcon,
    TagIcon,
    ThumbsDownIcon,
    ThumbsUpIcon,
    TimeIcon,
    ViewIcon,
    WifiIcon,
    ChevronRight,
};
