import React, { ReactNode } from "react";
import { useState } from "react";
import { Helmet } from "react-helmet";
import { useHistory } from "react-router-dom";

const urlPaths = [
    {
        title: "Branding",
        url: "/",
        children: [
            {
                title: "Color Themes",
                url: "",
            },
            {
                title: "Desktop Typography",
                url: "desktop-typography",
            },
            {
                title: "Mobile Typography",
                url: "mobile-typography",
            },
            {
                title: "Icons",
                url: "icons",
            },
        ],
    },
    {
        title: "Components",
        url: "/components/",
        children: [
            {
                title: "Buttons",
                url: "",
            },
            {
                title: "Text Inputs",
                url: "textinputs",
            },
            {
                title: "Custom Inputs",
                url: "custominputs",
            },
        ],
    },
];

export default function Layout({
    title,
    subTitle,
    children,
}: {
    title: string;
    children: ReactNode;
    subTitle: string;
}) {
    return (
        <div className="main">
            <Helmet title={title}>
                <link rel="preconnect" href="https://fonts.gstatic.com" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap"
                    rel="stylesheet"
                />
            </Helmet>
            <div className="header">
                <div className="header__subtitle">
                    <p className="color-label text--xs">{subTitle} | </p>
                    <RouteBreadCrumbs />
                </div>
                <h1 className="header__title heading heading--bold">{title}</h1>
            </div>
            <div className="main__content">{children}</div>
        </div>
    );
}

const RouteBreadCrumbs = () => {
    const history = useHistory();
    const splitPathName = history.location.pathname.split("/");
    const parentSelectDefaultValue =
        splitPathName[1] === "components" ? `/components/` : `/`;

    const [activeParentRoute, setActiveParentRoute] = useState(
        urlPaths.findIndex(
            (item) =>
                item.url ===
                (splitPathName[1] === "components" ? "/components/" : "/")
        )
    );

    const parentRoutes = urlPaths.map((item) => (
        <option key={`breadcrumb-${item.url}`} value={item.url}>
            {item.title}
        </option>
    ));

    const childRouteOptions = urlPaths[activeParentRoute].children.map(
        (item) => {
            const fullUrl = urlPaths[activeParentRoute].url + item.url;

            return (
                <option key={fullUrl} value={fullUrl}>
                    {item.title}
                </option>
            );
        }
    );

    return (
        <div className="header__breadcrumb">
            <div className="header__breadcrumb__item">
                <select
                    onChange={(e) => {
                        setActiveParentRoute(e.target.selectedIndex);
                        history.push(urlPaths[e.target.selectedIndex].url);
                    }}
                    defaultValue={parentSelectDefaultValue}
                >
                    {parentRoutes}
                </select>
            </div>
            <div className="header__breadcrumb__item">
                <select
                    onChange={(e) => {
                        const to =
                            urlPaths[activeParentRoute].url +
                            urlPaths[activeParentRoute].children[
                                e.target.selectedIndex
                            ].url;
                        history.push(to);
                    }}
                    defaultValue={history.location.pathname}
                >
                    {childRouteOptions}
                </select>
            </div>
        </div>
    );
};
