import React from "react";
import { CheckIcon } from "components/icons";

export type CheckBoxProps = {
    parentClassName?: string;
} & React.InputHTMLAttributes<HTMLInputElement>;

export function CheckBox({ parentClassName, ...props }: CheckBoxProps) {
    const parentClassNames = ["checkbox"];

    if (parentClassName) parentClassNames.push(parentClassName);
    return (
        <div className={parentClassNames.join(" ")}>
            <input type="checkbox" className="checkbox__input" {...props} />
            <span className="checkbox__visual">
                <CheckIcon className="checkbox__checkicon" />
            </span>
        </div>
    );
}
