import React from "react";

export type ToggleButtonProps = {
    parentClassName?: string;
} & React.InputHTMLAttributes<HTMLInputElement>;

export function ToggleButton({ parentClassName, ...props }: ToggleButtonProps) {
    const parentClassNames = ["toggle"];

    if (parentClassName) parentClassNames.push(parentClassName);
    return (
        <div className={parentClassNames.join(" ")}>
            <input type="checkbox" className="toggle__input" {...props} />
            <span className="toggle__visual"></span>
        </div>
    );
}
