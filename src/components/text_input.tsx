import React, { ReactNode, useEffect } from "react";
import { useRef } from "react";
import { useState } from "react";
import { CloseIcon } from "./icons";

export type TextInputProps = {
    label: string;
    icon?: ReactNode;
    parentClassName?: string;
    caption?: string;
    inputState?: "normal" | "success" | "error";
    inputSize?: "large" | "small";
} & React.InputHTMLAttributes<HTMLInputElement>;

export function TextInput({
    label,
    icon,
    parentClassName,
    caption,
    inputState = "normal",
    inputSize = "large",
    ...props
}: TextInputProps) {
    const text = props.value || props.defaultValue;

    const [hasText, setHasText] = useState(text ? true : false);
    const [focused, setFocused] = useState(false);
    const [showCloseButton, setShowCloseButton] = useState(false);

    useEffect(() => {
        let timeout: NodeJS.Timeout | null = null;

        if (focused && !showCloseButton) {
            setShowCloseButton(true);
        }

        if (!focused && showCloseButton) {
            timeout = setTimeout(() => {
                setShowCloseButton(false);
            }, 500);
        }

        return () => {
            if (timeout) clearTimeout(timeout);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [focused]);

    const parentClassNames = [
        "textinput",
        ...(hasText ? ["textinput--with-text"] : []),
        ...(focused ? ["textinput--focused"] : []),
    ];

    if (inputSize) parentClassNames.push(`textinput--${inputSize}`);

    if (parentClassName) parentClassNames.push(parentClassName);

    if (props.disabled) parentClassNames.push("textinput--disabled");

    if (inputState) parentClassNames.push(`textinput--state-${inputState}`);

    const inputRef = useRef<HTMLInputElement>(null);

    return (
        <div className={parentClassNames.join(" ")}>
            <div className="textinput__field">
                {icon && <div className="textinput__iconleft">{icon}</div>}
                <label className="textinput__label">
                    <input
                        ref={inputRef}
                        className="textinput__input"
                        {...props}
                        onChange={(e) => {
                            if (e.target.value !== "") {
                                if (!hasText) setHasText(true);
                            } else {
                                if (hasText) setHasText(false);
                            }
                            if (props.onChange) props.onChange(e);
                        }}
                        onFocus={(e) => {
                            setFocused(true);
                            if (props.onFocus) props.onFocus(e);
                        }}
                        onBlur={(e) => {
                            setFocused(false);
                            if (props.onBlur) props.onBlur(e);
                        }}
                    />
                    <span className="textinput__title">{label}</span>
                </label>
                {showCloseButton && (
                    <button
                        className="textinput__closeicon"
                        onClick={() => {
                            if (inputRef.current) {
                                inputRef.current.value = "";
                                setHasText(false);
                            }
                        }}
                    >
                        <CloseIcon />
                    </button>
                )}
            </div>
            {caption && (
                <div className="textinput__caption">
                    <p>{caption}</p>
                </div>
            )}
        </div>
    );
}
