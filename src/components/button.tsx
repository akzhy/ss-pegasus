import React, { useEffect } from "react";
import { useState } from "react";
import { useRef } from "react";
import { ChevronRight, LoadingIcon } from "./icons";

export type ButtonProps = {
    label?: string;
    actionType?: "primary" | "secondary" | "subtle" | "text";
    isLoading?: boolean;
    size?: "large" | "medium" | "small";
    className?: string;
    iconOnly?: boolean;
} & React.ButtonHTMLAttributes<HTMLButtonElement>;

export function Button({
    label,
    actionType = "primary",
    type,
    isLoading = false,
    size = "large",
    iconOnly = false,
    className,
    ...props
}: ButtonProps) {
    const classNames = ["button"];
    classNames.push(`button--${actionType}`);
    classNames.push(`button--size-${size}`);

    if (isLoading) classNames.push(`button--loading`);

    if (iconOnly) classNames.push("button--icon-only");

    if (props.disabled) classNames.push("button--disabled");

    if (className) classNames.push(className);

    const ripplePosition = useRef({
        x: 0,
        y: 0,
    });

    const [rippleShown, setRippleShown] = useState(false);

    useEffect(() => {
        let timeout: NodeJS.Timeout | null = null;

        if (rippleShown) {
            timeout = setTimeout(() => {
                setRippleShown(false);
            }, 450);
        }

        return () => {
            if (timeout) clearTimeout(timeout);
        };
    }, [rippleShown]);

    return (
        <button
            className={classNames.join(" ")}
            type={type}
            disabled={props.disabled}
            {...props}
            onClick={(e) => {
                const target = e.target as HTMLButtonElement;
                const bounds = target.getBoundingClientRect();
                const xPos = e.pageX - (window.scrollX + bounds.left);
                const yPos = e.pageY - (window.scrollY + bounds.top);

                ripplePosition.current = {
                    x: xPos - 32,
                    y: yPos - 32,
                };

                setRippleShown(true);

                if (props.onClick) props.onClick(e);
            }}
        >
            {rippleShown && (
                <span
                    className="button__ripple"
                    style={{
                        left: ripplePosition.current.x,
                        top: ripplePosition.current.y,
                    }}
                ></span>
            )}
            {(() => {
                if (isLoading) {
                    if (actionType === "text") return "Loading...";
                    return <LoadingIcon />;
                }

                if (iconOnly)
                    return <ChevronRight className="button__chevron" />;

                return label;
            })()}
        </button>
    );
}
