import React, { ReactNode } from "react";

export default function Section({
    children,
    title,
}: {
    children: ReactNode;
    title: string;
}) {
    return (
        <div className="section">
            <h2 className="section__title heading heading--md heading--bold">
                {title}
            </h2>
            <div className="section__content">{children}</div>
        </div>
    );
}
