import React, { useEffect } from "react";
import { useRef } from "react";
import { useState } from "react";
import { CloseIcon } from "./icons";

export type TextAreaProps = {
    label: string;
    parentClassName?: string;
    caption?: string;
    inputState?: "normal" | "success" | "error";
} & React.TextareaHTMLAttributes<HTMLTextAreaElement>;

export function TextArea({
    label,
    parentClassName,
    caption,
    inputState = "normal",
    ...props
}: TextAreaProps) {
    const text = props.value || props.defaultValue;

    const [hasText, setHasText] = useState(text ? true : false);
    const [focused, setFocused] = useState(false);
    const [showCloseButton, setShowCloseButton] = useState(false);

    useEffect(() => {
        let timeout: NodeJS.Timeout | null = null;

        if (focused && !showCloseButton) {
            setShowCloseButton(true);
        }

        if (!focused && showCloseButton) {
            timeout = setTimeout(() => {
                setShowCloseButton(false);
            }, 500);
        }

        return () => {
            if (timeout) clearTimeout(timeout);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [focused]);

    const parentClassNames = [
        "textarea",
        ...(hasText ? ["textarea--with-text"] : []),
        ...(focused ? ["textarea--focused"] : []),
    ];

    if (parentClassName) parentClassNames.push(parentClassName);

    if (props.disabled) parentClassNames.push("textarea--disabled");

    if (inputState) parentClassNames.push(`textarea--state-${inputState}`);

    const inputRef = useRef<HTMLTextAreaElement>(null);

    return (
        <div className={parentClassNames.join(" ")}>
            <div className="textarea__field">
                <label className="textarea__label">
                    <textarea
                        ref={inputRef}
                        className="textarea__input"
                        {...props}
                        onChange={(e) => {
                            if (e.target.value !== "") {
                                if (!hasText) setHasText(true);
                            } else {
                                if (hasText) setHasText(false);
                            }
                            if (props.onChange) props.onChange(e);
                        }}
                        onFocus={(e) => {
                            setFocused(true);
                            if (props.onFocus) props.onFocus(e);
                        }}
                        onBlur={(e) => {
                            setFocused(false);
                            if (props.onBlur) props.onBlur(e);
                        }}
                    />
                    <span className="textarea__title">{label}</span>
                </label>
                {showCloseButton && (
                    <button
                        className="textarea__closeicon"
                        onClick={() => {
                            if (inputRef.current) {
                                inputRef.current.value = "";
                                setHasText(false);
                            }
                        }}
                    >
                        <CloseIcon />
                    </button>
                )}
            </div>
            {caption && (
                <div className="textarea__caption">
                    <p>{caption}</p>
                </div>
            )}
        </div>
    );
}
