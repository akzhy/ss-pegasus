import React from "react";

export type RadioButtonProps = {
    parentClassName?: string;
} & React.InputHTMLAttributes<HTMLInputElement>;

export function RadioButton({ parentClassName, ...props }: RadioButtonProps) {
    const parentClassNames = ["radio"];

    if (parentClassName) parentClassNames.push(parentClassName);
    return (
        <div className={parentClassNames.join(" ")}>
            <input type="radio" className="radio__input" {...props} />
            <span className="radio__visual"></span>
        </div>
    );
}
