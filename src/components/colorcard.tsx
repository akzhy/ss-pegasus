import React from "react";

export interface ColorCardItem {
    title: string;
    subTitle: string;
    bgClass: string;
    variantColors?: [string, string];
}

export function ColorCard({
    title,
    subTitle,
    bgClass,
    variantColors,
}: ColorCardItem) {
    return (
        <div className="colorcards__item">
            <div className={`colorcards__item__color ${bgClass}`}>
                {variantColors && (
                    <React.Fragment>
                        <div
                            className={`colorcards__item__variant ${variantColors[0]}`}
                        ></div>
                        <div
                            className={`colorcards__item__variant ${variantColors[1]}`}
                        ></div>
                    </React.Fragment>
                )}
            </div>
            <div className="colorcards__item__content">
                <p className="text--lg text--bold color-title-active">
                    {title}
                </p>
                <p className="text color-label">{subTitle}</p>
            </div>
        </div>
    );
}

export function ColorCards({ data }: { data: ColorCardItem[] }) {
    const items = data.map((item, i) => (
        <ColorCard {...item} key={`colorcard-${i}`} />
    ));

    return <div className="colorcards">{items}</div>;
}
